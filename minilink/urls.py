from django.urls import path
from minilink import views

urlpatterns = [
    path('', views.index, name='home'),
    path('<slug:short>', views.redirect_full, name='redirect'),
]