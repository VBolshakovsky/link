from django.apps import AppConfig


class MinilinkConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'minilink'
