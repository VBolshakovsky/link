from django.contrib import admin
from .models import Urls

# Register your models here.
class UrlsAdmin(admin.ModelAdmin):
    list_display = ('id','short_full_admin','short','date','count')
    list_display_links = ('id','short','date')
    search_fields = ('full','short')
    list_editable = ('count',)
    list_filter = ('date','count')

    def upper_case_name(self, obj):
        return ("%s %s" % (obj.first_name, obj.last_name)).upper()
    upper_case_name.short_description = 'Name'

admin.site.register(Urls,UrlsAdmin)

admin.site.site_title = 'Админка minilink'
admin.site.site_header = 'minilink.Bolshakovsky'