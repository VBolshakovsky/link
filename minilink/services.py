from datetime import datetime
import string, random
from .models import Urls

def get_url(full):
    # Функция возвращает заполненный обьект url
    url = Urls()
    url.full = full
    url.short = get_short()
    url.date = datetime.now()

    return url

def get_short():
    # Функция возвращает короткую ссылку, минимизирую длинну.
    # Принцип работы:
    # Составим список возможных символов (baseSymbols). Где будут прописные букви малые большие и цифры
    # Найдем нужную длинну ссылки(lenSymbols), используя подсчет возможных комбинаций: xxx - кол-во комбинаций = (разрядность x)**(кол-во x)
    # В цикле переберем варианты пока не надем свободный(не самый лучший алгоритм).
    
    baseSymbols = string.ascii_uppercase + string.digits + string.ascii_lowercase
    
    if Urls.objects.count() == 0:
        lenSymbols = 1
    else:
        LastUrls = Urls.objects.all().order_by('-id').first()
        maxLenShort = int(len(LastUrls.short))
            
        if Urls.objects.count() <= len(baseSymbols)**maxLenShort:
            lenSymbols = maxLenShort
        else:
            lenSymbols = maxLenShort + 1
    
    while True:
        short = ''.join(random.choices(baseSymbols, k=lenSymbols))

        if not Urls.objects.filter(short=short):
            return short

def get_client_ip(request):
    # Функция возвращает IP клиента
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_client_agent(request):
    # Функция возвращает приложение клиента
    return request.META['HTTP_USER_AGENT']