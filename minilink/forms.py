from django import forms
from django.forms import ModelForm, widgets
from .models import Urls
from django.core.exceptions import ValidationError

class AddURLForm(ModelForm):
    
    class Meta:
        model = Urls
        fields = ['full','short']
        widgets = {
            'full': forms.URLInput(attrs={'placeholder':"Скопируй сюда ссылку", 'autofocus':"autofocus", "required":"required"})
            }

    def clean_full(self):
        full = self.cleaned_data['full']
        if len(full) > 200:
            raise ValidationError('Длина превышает 200 символов')
        return full