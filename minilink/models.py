from django.urls import reverse
from django.template.defaultfilters import truncatechars
from django.db import models

class Urls(models.Model):
    short = models.SlugField(max_length=6, unique=True, blank=True, null=True, verbose_name='Символьный URL')
    full = models.URLField(max_length=200, blank=True, null=True, verbose_name='Полный URL')
    date = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name='Дата добавления')
    date_used = models.DateTimeField(auto_now=True, blank=True, null=True, verbose_name='Последнее использование')
    count = models.IntegerField(default=0, blank=True, null=True, verbose_name='Переходов')
    ip = models.CharField(max_length=20, blank=True, null=True, verbose_name='IP')
    agent = models.CharField(max_length=300, blank=True, null=True, verbose_name='agent')
    
    def __str__(self):
        return self.full

    def get_absolute_url(self):
        return reverse('redirect', kwargs={'short_url':self.short})

    def short_full_admin(self):
        return truncatechars(self.full, 30)

    class Meta:
        verbose_name = 'URL'
        verbose_name_plural = 'URL'
        ordering = ['-date']