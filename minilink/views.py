from datetime import datetime
from django.shortcuts import render
from django.urls import reverse
from .services import get_url, get_client_ip, get_client_agent
from .models import Urls
from .forms import AddURLForm
from django.shortcuts import get_object_or_404, render
from django.http.response import HttpResponseRedirect

def index(request):
    context = {}
    iscontent = False
    short = ''
    
    if request.POST.get('reduce'):
        iscontent = True
        URLForm = AddURLForm(request.POST)
        
        if URLForm.is_valid():
            full = URLForm.cleaned_data.get('full')

            if Urls.objects.filter(full = full):
                url = Urls.objects.get(full = full)
            else:
                url = get_url(full)
                url.ip = get_client_ip(request)
                url.agent = get_client_agent(request)
                url.save()
            
            short = request.build_absolute_uri(reverse('home')) + url.short
    else:        
        URLForm = AddURLForm()
        
    context['short'] = short
    context['URLForm'] = URLForm
    context['iscontent'] = iscontent
    
    return render(request,'minilink/index.html', context)

def redirect_full(request, short):
    context = {}
    
    urlNotFound = request.build_absolute_uri(reverse('home')) + short
    
    try:
        url = Urls.objects.get(short=short)
        
        url.count += 1
        url.date_used = datetime.now()
        url.save
        
    except Urls.DoesNotExist:
        
        context['urlNotFound'] = urlNotFound
        return render(request,'minilink/not_found.html', context)

    return HttpResponseRedirect(url.full)