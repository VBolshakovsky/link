from django import forms
from django.forms import ModelForm, widgets
from .models import Urls
from django.core.exceptions import ValidationError

class AddURLForm(ModelForm):
    class Meta:
        model = Urls
        fields = ['full_url','short_symbol_url']
        widgets = {
            'full_url': forms.URLInput(attrs={'class': 'form-input','autofocus': ""})
            }
    def clean_full_url(self):
        full_url = self.cleaned_data['full_url']
        if len(full_url) > 200:
            raise ValidationError('Длина превышает 200 символов')

        return full_url
