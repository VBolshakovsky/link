Описание:
---------------------------------
Сервис по сокращению ссылок. Формирует короткую ссылку, перейдя на которую происходит переадрессация на оргинал. Причем формирует новую ссылку в зависимости от заполненности базы. Сначала 1 разрядную, затем 2 разрядную и так далеее... Сохраняет, для аналитики IP и параметры устройства клиента.

Ссылка:
---------------------------------
https://link.bolshakovsky.ru/

Запуск(Общие шаги):
---------------------------------

GIT
1.	Установить Git
    https://github.com/cyberspacedk/Git-commands

    Для Windows:
    включить преобразование окончаний строк из CRLF в LF
    git config --global core.autocrlf true 

2.	Настроить Git

    указать имя, которым будут подписаны коммиты
    git config --global user.name "@Pupkin"

    указать электропочту
    git config --global user.email vasyPupkin@gmail.ru

3.	Клонировать
    git clone https://github.com/vasyPupkin/project.git

PYTHON
1.	Установить python
    https://www.python.org/downloads/

2.	Создадим виртуальное окружение
    python3 -m venv venv

    Для Windows:
    python -m venv venv

3.	Активируем
    source venv/bin/activate

    Для Windows:
    .\venv\Scripts\Activate.ps1

4.	Обновим pip
    pip install --upgrade pip

DJANGO
1.	Установим зависимости
    pip install -r requirements.txt

2.	Создадим и применим миграции
    python manage.py makemigrations
    python manage.py migrate

DYNACONF
1.	Создать в директории с manage.py файл
    .secrets.yaml

2.	Заполнить .secrets.yaml своими настройками, например:

    development:
    SECRET_KEY: 'django-insecure-bzc0ejxswto^6_0j!#l!=z^*ozfs+$xy@jsadasdasdw9yvl'
    ADMIN_URL: admin/
    # Database
    DATABASES:
        default:
        'ENGINE': 'django.db.backends.sqlite3'
        NAME: BASE_DIR / 'db.sqlite3'

3.	Проверить что в .env,  включена нужная конфигурация
    export ENV_FOR_DYNACONF=development